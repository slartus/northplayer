package org.softeg.northplayer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by slartus on 05.02.14.
 */
public class PlayInfo implements Parcelable {
    private final CharSequence apiId;
    private final CharSequence id;
    private final CharSequence title;
    private final CharSequence requestUrl;
    private final CharSequence videoUrl;

    public PlayInfo(CharSequence apiId, CharSequence id, CharSequence title,
                    CharSequence requestUrl, CharSequence videoUrl) {

        this.apiId = apiId;
        this.id = id;
        this.title = title;
        this.requestUrl = requestUrl;
        this.videoUrl = videoUrl;
    }

    public CharSequence getApiId() {
        return apiId;
    }

    public CharSequence getId() {
        return id;
    }

    public CharSequence getTitle() {
        return title;
    }

    public CharSequence getRequestUrl() {
        return requestUrl;
    }

    public CharSequence getVideoUrl() {
        return videoUrl;
    }

    public static final Parcelable.Creator<PlayInfo> CREATOR
            = new Parcelable.Creator<PlayInfo>() {
        public PlayInfo createFromParcel(Parcel in) {
            return new PlayInfo(in);
        }

        public PlayInfo[] newArray(int size) {
            return new PlayInfo[size];
        }
    };

    private PlayInfo(Parcel parcel) {
        apiId = parcel.readString();
        id = parcel.readString();
        title = parcel.readString();
        requestUrl = parcel.readString();
        videoUrl = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(apiId.toString());
        parcel.writeString(id.toString());
        parcel.writeString(title.toString());
        parcel.writeString(requestUrl.toString());
        parcel.writeString(videoUrl.toString());
    }
}
