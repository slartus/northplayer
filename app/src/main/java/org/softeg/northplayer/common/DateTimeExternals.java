package org.softeg.northplayer.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by slinkin on 05.02.14.
 */
public class DateTimeExternals {
    //http://youtu.be/bnvNEfMOORU?t=1m29s
    public static String getHumanTime(long miliseconds){
        if(miliseconds<1000*60){
            return new SimpleDateFormat("ss's'").format(new Date(miliseconds));
        }
        if(miliseconds<1000*60*60){
            return new SimpleDateFormat("mm'm'ss's'").format(new Date(miliseconds));
        }

        return new SimpleDateFormat("hh'h'mm'm'ss's'").format(new Date(miliseconds));

    }
}
