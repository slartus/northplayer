package org.softeg.northplayer.common;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import org.softeg.northplayer.Log;
import org.softeg.northplayer.bridges.ClipboardBridge;

/**
 * Created by slinkin on 06.02.14.
 */
public class UriExtensions {
    public static boolean tryParse(String uriString, StringBuilder sb){
        try {
            sb.append(Uri.parse(uriString));
            return true;
        }catch (Throwable ignored){
            return false;
        }
    }

    public static void showInBrowser(Context context, String url) {
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(Intent.createChooser(marketIntent, "Выберите"));
    }

    public static void shareIt(Context context, String url) {
        Intent sendMailIntent = new Intent(Intent.ACTION_SEND);
        sendMailIntent.putExtra(Intent.EXTRA_SUBJECT, url);
        sendMailIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendMailIntent.setType("text/plain");

        context.startActivity(Intent.createChooser(sendMailIntent, "Поделиться через.."));
    }

    public static void copyLinkToClipboard(Context context, String link) {
        ClipboardBridge.setText(context,link);
        Toast.makeText(context, "Ссылка скопирована в буфер обмена", Toast.LENGTH_SHORT).show();
    }
}
