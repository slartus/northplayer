package org.softeg.northplayer.common;

/**
 * Created by slinkin on 03.02.14.
 */
public class ArrayExternals {
    public static int indexOf(int needle, int[] haystack) {
        for (int i = 0; i < haystack.length; i++) {
            if (haystack[i] == needle) return i;
        }

        return -1;
    }
}
