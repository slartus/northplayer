package org.softeg.northplayer;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.text.TextUtils;

import org.softeg.northplayer.db.Contract;

import java.util.Calendar;

/**
 * Created by slinkin on 03.02.14.
 */
public class History {
    private Context context;
    private static final int MAX_HISTORY_COUNT = 20;


    public History(Context context) {
        this.context = context;
    }

    private Context getContext() {
        return context;
    }

    public void addToHistory(CharSequence id, CharSequence title, CharSequence url, CharSequence apiId) {
        Cursor cursor = null;
        try {

            cursor = context.getContentResolver().query(Contract.History.CONTENT_URI, null,
                    Contract.History.ID_COLUMN + "=? AND " + Contract.History.SITE_COLUMN + "=?",
                    new String[]{id.toString(), apiId.toString()},
                    Contract.History.DEFAULT_SORT_ORDER);

            ContentValues cv = new ContentValues();
            cv.put(Contract.History.ID_COLUMN, id.toString());
            cv.put(Contract.History.DATE_COLUMN, Calendar.getInstance().getTimeInMillis());
            cv.put(Contract.History.SITE_COLUMN, apiId.toString());
            cv.put(Contract.History.URL_COLUMN, url.toString());

            if(!TextUtils.isEmpty(title))
                cv.put(Contract.History.TITLE_COLUMN, title.toString());

            if (cursor != null && cursor.moveToFirst()) {
                long l = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
                context.getContentResolver().update(ContentUris.withAppendedId(Contract.History.CONTENT_URI, l), cv, null, null);
            } else {
                context.getContentResolver().insert(Contract.History.CONTENT_URI, cv);
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }
        removeLimited();
    }

    private void removeLimited() {
        Cursor cursor = null;
        try {

            // получаем последние N записей
            cursor = context.getContentResolver().query(Contract.History.CONTENT_URI, null, null, null,
                    Contract.History.DEFAULT_SORT_ORDER + " LIMIT " + MAX_HISTORY_COUNT);
            if (cursor != null && cursor.getCount() > 0) {
                // передвигаем курсов на последнюю
                cursor.moveToPosition(cursor.getCount() - 1);
                long dateTimeMs = cursor.getLong(cursor.getColumnIndexOrThrow(Contract.History.DATE_COLUMN));

                // удаляем все записи, дата который меньше, чем последняя, нужная нам
                context.getContentResolver().delete(Contract.History.CONTENT_URI,
                        Contract.History.DATE_COLUMN + "<?", new String[]{Long.toString(dateTimeMs)});
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }
}
