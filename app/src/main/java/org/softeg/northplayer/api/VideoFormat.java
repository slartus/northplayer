package org.softeg.northplayer.api;

import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Format in the "fmt_list" parameter
 */
public class VideoFormat {

    private CharSequence mUrl;
    private CharSequence title;

    public void setParams(String params) {
        Matcher m = Pattern.compile("(\\w+)=([^&]*)").matcher(params);
        String sig = null;
        String url = null;
        while (m.find()) {
            String name = m.group(1);
            if (url == null && "url".equalsIgnoreCase(name))
                url = m.group(2);
            else if (sig == null && "sig".equalsIgnoreCase(name))
                sig = m.group(2);
            if (url != null && sig != null)
                break;
        }

        mUrl = url + "&signature=" + sig;
    }

    public CharSequence getUrl() {
        return mUrl;
    }

    public void setUrl(CharSequence url) {
        mUrl = url;
    }

    public CharSequence getTitle() {
        return title;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }
}
