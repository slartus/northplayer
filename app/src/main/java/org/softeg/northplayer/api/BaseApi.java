package org.softeg.northplayer.api;

import java.io.IOException;

/**
 * Created by slartus on 04.02.14.
 */
public abstract class BaseApi {
    public abstract CharSequence getApiId();
    public static BaseApi getApi(CharSequence url) throws Exception {
        if(YouTubeAPI.isOwner(url))
            return new YouTubeAPI();
        if(CarambaApi.isOwner(url))
            return new CarambaApi();
        throw new Exception("Сервис не определён!");
    }

    public static BaseApi getApiById(CharSequence id) throws Exception {
        if(YouTubeAPI.ID.equals(id))
            return new YouTubeAPI();
        if(CarambaApi.ID.equals(id))
            return new CarambaApi();
        throw new Exception("Сервис не определён!");
    }

    public abstract ParseResult parse(CharSequence url) throws Exception;

    public abstract CharSequence getPositionUrl(CharSequence videoUrl, int positionMs);
}
