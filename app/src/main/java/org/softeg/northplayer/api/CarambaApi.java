package org.softeg.northplayer.api;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;
import org.softeg.northplayer.common.Http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by slinkin on 04.02.14.
 */
public class CarambaApi extends BaseApi {


    public static final CharSequence ID = "carambatv.ru";

    static Boolean isOwner(CharSequence url) {
        return isCarambaUrl(url);
    }

    public static boolean isCarambaUrl(CharSequence url) {
        return Pattern.compile("carambatv.ru", Pattern.CASE_INSENSITIVE).matcher(url).find();
    }

    public static CharSequence getId(CharSequence url) throws Exception {
        Matcher m = Pattern.compile("video1.carambatv.ru/v/(\\d+)", Pattern.CASE_INSENSITIVE)
                .matcher(URLDecoder.decode(url.toString(), "UTF-8"));
        if (!m.find())
            return null;

        return m.group(1);
    }


    @Override
    public CharSequence getApiId() {
        return ID;
    }

    private Boolean checkIsListPage(String page){
        return Pattern.compile("class=\"videos\"",Pattern.CASE_INSENSITIVE).matcher(page).find();
    }

    @Override
    public ParseResult parse(CharSequence url) throws Exception {
        Matcher m = Pattern.compile("exc=(http:[^:]*carambatv.ru/(?:[^:/]*/)*)").matcher(URLDecoder.decode(url.toString(), "UTF-8"));
        if (m.find())
            url = m.group(1);

        ParseResult carambaInfo = new ParseResult();
        carambaInfo.setRequestUrl(url);
        carambaInfo.setTitle(url);
        CharSequence id = getId(url);
        if (id == null) {
            tryGetVideoPage(carambaInfo, Http.getPage(url.toString(), "UTF-8"));
        } else {
            parseVideoInfo(carambaInfo, id);
            //parseVideoPage(carambaInfo, page);
        }


//        String page = Http.getPage(url.toString(), "UTF-8");
//        if (!isVideoPageUrl(url)) {
//            tryGetVideoPage(carambaInfo, page);
//        } else {
//            parseVideoPage(carambaInfo, page);
//        }


        return carambaInfo;
    }

    @Override
    public CharSequence getPositionUrl(CharSequence videoUrl, int positionMs) {
        return videoUrl + "?start=" + positionMs / 1000.0;
    }

    private void getQualities(ParseResult carambaInfo, CharSequence url) throws Exception {
        CharSequence[] qualities = {"240", "360", "480", "720"};
        Matcher m;
        m = Pattern.compile("carambatv.ru/v/(\\d+)", Pattern.CASE_INSENSITIVE).matcher(url);
        if (m.find()) {
            CharSequence id = m.group(1);
            for (CharSequence quality : qualities) {
                CharSequence qualityUrl = String.format("http://video1.carambatv.ru/v/%s/%s.mp4", id, quality);
                if (Http.ping(qualityUrl.toString())) {
                    VideoFormat format = new VideoFormat();
                    format.setUrl(qualityUrl);
                    format.setTitle(quality);
                    carambaInfo.getFormats().add(format);
                }
            }
        }
        carambaInfo.setVideoUrl(url);
        carambaInfo.setId(getId(url));
    }

    private void parseVideoInfo(ParseResult carambaInfo, CharSequence id) throws Exception {
        String page = Http.getPage(String.format("http://video1.carambatv.ru/v/%s/videoinfo.js", id), "UTF-8");
        JSONObject jObj = new JSONObject(page);
        carambaInfo.setId(id);
        carambaInfo.setTitle(jObj.getString("title"));

        if (jObj.has("qualities")) {
            JSONArray qualitiesArray = jObj.getJSONArray("qualities");

            for (int i = 0; i < jObj.getJSONArray("qualities").length(); i++) {
                JSONObject qualityObject = qualitiesArray.getJSONObject(i);
                CharSequence qualityUrl = String.format("http://video1.carambatv.ru/v/%s/%s", id, qualityObject.getString("fn"));
                CharSequence qualityTitle = qualityObject.getString("height") +
                        (qualityObject.getInt("hd") == 1 ? " HD" : "");
                VideoFormat format = new VideoFormat();
                format.setUrl(qualityUrl);
                format.setTitle(qualityTitle);
                carambaInfo.getFormats().add(format);
            }
        }

        if (jObj.has("default_bitrate")) {
            carambaInfo.setVideoUrl(String.format("http://video1.carambatv.ru/v/%s/%s", id, jObj.getString("default_bitrate")));
        }
    }

    private void parseVideoPage(ParseResult carambaInfo, String page) throws Exception {
        Matcher m;
        m = Pattern.compile("<source src=\"([^\"]*)\"", Pattern.CASE_INSENSITIVE)
                .matcher(page);
        if (!m.find())
            throw new Exception("Не могу получить ссылку на видео carambatv.ru");
        carambaInfo.setVideoUrl(m.group(1));
        carambaInfo.setId(getId(m.group(1)));
    }

    private void tryGetVideoPage(ParseResult carambaInfo, String page) throws Exception {
        Matcher m = Pattern.compile("<section id=\"video-container\">[\\s\\S]*?<iframe src=\"//([^\"]*)\"", Pattern.CASE_INSENSITIVE)
                .matcher(page);
        if (!m.find()){
            if(checkIsListPage(page)){
                parseVideoList(carambaInfo,page);
                return;
            }
            throw new Exception("Не могу получить ссылку на страницу с видео carambatv.ru");
        }
        parseVideoInfo(carambaInfo, getId(m.group(1)));
    }

    private CharSequence getTitleFromTitleTag(String page){
        String[] titlePatterns = {
                "<title>([^<]*?)&raquo; &quot;CarambaTV&quot; - Смотреть новые серии онлайн</title>",
                "<title>([^<]*)</title>",
                "<meta property=\"og:title\" content=\"([^\"]*)\" />",};
        for (String titlePattern : titlePatterns) {
            Matcher m = Pattern.compile(titlePattern, Pattern.CASE_INSENSITIVE)
                    .matcher(page);
            if (!m.find()) continue;
            return m.group(1);

        }
        return "";
    }

    private void parseVideoList(ParseResult carambaInfo, String page){
        carambaInfo.setTitle(getTitleFromTitleTag(page));

        Matcher m = Pattern.compile("<a class=\"name\" href=\"([^\"]*)\">([^<]*)</a>", Pattern.CASE_INSENSITIVE)
                .matcher(page);
        while (m.find()){
            VideoListItem item=new VideoListItem();
            item.setRequestUrl(m.group(1));
            item.setTitle(m.group(2));
            carambaInfo.getItemsList().add(item);
        }
    }

    private Boolean isVideoPageUrl(CharSequence url) throws Exception {
        return getId(url) != null;
    }
}
