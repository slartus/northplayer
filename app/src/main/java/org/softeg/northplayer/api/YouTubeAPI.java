package org.softeg.northplayer.api;

import org.softeg.northplayer.common.ArrayExternals;
import org.softeg.northplayer.common.Http;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YouTubeAPI extends BaseApi {

    public static final CharSequence ID = "youtube.com";


    public static CharSequence getYoutubeId(CharSequence youtubeUrl) {
        String[] patterns = {
                "v=([^&?#]*)",
                "youtu.be/([^/?&#]*)",
                "vnd.youtube:([^/?&#]*)",
                "embed/([^&?#]*)",
        };
        for (String pattern : patterns) {
            Matcher m = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(youtubeUrl);
            if (m.find())
                return m.group(1);
        }
        return null;
    }

    public static Boolean isYoutubeUrl(CharSequence url) {
        return Pattern.compile("youtube.com|youtu.be|vnd.youtube", Pattern.CASE_INSENSITIVE).matcher(url).find();
    }

    public static final String URL_GET_VIDEO_INFO = "http://www.youtube.com/get_video_info?&video_id=";


    static Boolean isOwner(CharSequence url) {
        return isYoutubeUrl(url);
    }

    @Override
    public CharSequence getApiId() {
        return ID;
    }

    @Override
    public ParseResult parse(CharSequence url) throws Exception {
        url = URLDecoder.decode(url.toString(), "UTF-8");

        ParseResult info = new ParseResult();
        info.setTitle(url);
        info.setRequestUrl(url);

        CharSequence id = getYoutubeId(url);
        info.setId(id);
        info.setTitle(id);

        url = URL_GET_VIDEO_INFO + id;
        info.setVideoUrl(url);

        String infoStr = Http.getPage(url.toString(), "UTF-8");
        if (infoStr == null)
            throw new Exception("Сервер вернул пустую страницу");

        Matcher m = Pattern.compile("([^&=]*)=([^$&]*)", Pattern.CASE_INSENSITIVE).matcher(infoStr);

        String fmtList = null;
        String url_encoded_fmt_stream_map = null;
        String title = null;
        while (m.find()) {
            String name = m.group(1);

            if (fmtList == null && "fmt_list".equalsIgnoreCase(name))
                fmtList = URLDecoder.decode(m.group(2), "UTF-8");
            else if (url_encoded_fmt_stream_map == null && "url_encoded_fmt_stream_map".equalsIgnoreCase(name))
                url_encoded_fmt_stream_map = URLDecoder.decode(m.group(2), "UTF-8");
            else if (title == null && "title".equalsIgnoreCase(name))
                title = URLDecoder.decode(m.group(2), "UTF-8");

            if (fmtList != null && url_encoded_fmt_stream_map != null && title != null)
                break;
        }

        info.setTitle(title);

        if (fmtList != null && url_encoded_fmt_stream_map != null) {
            Matcher fmtMatcher = Pattern.compile("(\\d+)/(\\d+x\\d+)/\\d+/\\d+/\\d+").matcher(fmtList);
            String streamStrs[] = url_encoded_fmt_stream_map.split(",");
            int fmtInd = 0;
            while (fmtMatcher.find()) {
                fmtInd++;
                int ind = ArrayExternals.indexOf(Integer.parseInt(fmtMatcher.group(1)), YouTubeFMTQuality.supported);
                if (ind == -1) continue;
                VideoFormat videoFormat = new VideoFormat();
                videoFormat.setTitle(YouTubeFMTQuality.supported_titles[ind]);
                videoFormat.setUrl(URLDecoder.decode(getUrlFromParams(streamStrs[fmtInd - 1]),"UTF-8"));

                info.getFormats().add(videoFormat);
            }
        }

        return info;
    }

    @Override
    public CharSequence getPositionUrl(CharSequence videoUrl, int positionMs) {
        return videoUrl;
//        if (videoUrl.toString().contains("?"))
//            return videoUrl + "&t=" + DateTimeExternals.getHumanTime(positionMs);
//        return videoUrl + "?t=" + DateTimeExternals.getHumanTime(positionMs);
    }

    private static String getUrlFromParams(CharSequence params) throws UnsupportedEncodingException {

        Matcher m = Pattern.compile("(\\w+)=([^&]*)").matcher(params);
        String sig = null;
        String quality = null;
        String url = null;
        while (m.find()) {
            String name = m.group(1);
            if (url == null && "url".equalsIgnoreCase(name))
                url = m.group(2);
            else if (sig == null && "sig".equalsIgnoreCase(name))
                sig = m.group(2);
            else if (quality == null && "quality".equalsIgnoreCase(name))
                quality = m.group(2);
            if (url != null && sig != null && quality != null)
                break;
        }

        return url + "&signature=" + sig;

    }
}
