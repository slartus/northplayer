package org.softeg.northplayer.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by slinkin on 03.02.14.
 */
public class Provider extends ContentProvider {

    private static final int HISTORY = 1;
    private static final int HISTORY_ID = 2;

    private static final UriMatcher sUriMatcher;
    private DbOpenHelper mOpenHelper;

    @Override
    public boolean onCreate() {
        mOpenHelper = new DbOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String groupBy = null;
        String defaultSortOrder;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (sUriMatcher.match(uri)) {
            case HISTORY:
                qb.setTables(Contract.History.TABLE_NAME);
                defaultSortOrder = Contract.History.DEFAULT_SORT_ORDER;
                break;
            case HISTORY_ID:
                qb.setTables(Contract.History.TABLE_NAME);
                defaultSortOrder = Contract.History.DEFAULT_SORT_ORDER;
                qb.appendWhere("_id=" + uri.getLastPathSegment());
                break;


            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = defaultSortOrder;
        } else {
            orderBy = sortOrder;
        }
        Cursor c = qb.query(mOpenHelper.getReadableDatabase(), projection, selection, selectionArgs, groupBy, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case HISTORY:
                return Contract.History.CONTENT_TYPE;
            case HISTORY_ID:
                return Contract.History.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String tableName;
        switch (sUriMatcher.match(uri)) {

            case HISTORY:
                tableName = Contract.History.TABLE_NAME;
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        long rowId = mOpenHelper.getWritableDatabase().insertOrThrow(tableName, null, values);
        if (rowId > 0) {
            Uri itemUri = ContentUris.withAppendedId(uri, rowId);
            getContext().getContentResolver().notifyChange(itemUri, null);
            return itemUri;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String tableName;
        switch (sUriMatcher.match(uri)) {

            case HISTORY_ID:
                tableName = Contract.History.TABLE_NAME;
                selection = "_id=" + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;

            case HISTORY:
                tableName = Contract.History.TABLE_NAME;
                break;


            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        int count = mOpenHelper.getWritableDatabase().delete(tableName, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String tableName;
        switch (sUriMatcher.match(uri)) {

            case HISTORY_ID:
                tableName = Contract.History.TABLE_NAME;
                selection = "_id=" + uri.getLastPathSegment() + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        int count = mOpenHelper.getWritableDatabase().update(tableName, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    static {

        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.History.TABLE_NAME, HISTORY);
        sUriMatcher.addURI(Contract.AUTHORITY, Contract.History.TABLE_NAME + "/#", HISTORY_ID);

    }

}
