package org.softeg.northplayer.db;

/**
 * Created by slinkin on 04.02.14.
 */
public interface IMainActivity {
    void startProgress();
    void stopProgress();
    void error(CharSequence error);
    void error(Throwable ex);
}
