package org.softeg.northplayer.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by slinkin on 03.02.14.
 */
public class DbOpenHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "syp.db";
    private static final int DATABASE_VERSION = 7;




    public DbOpenHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE " + Contract.History.TABLE_NAME + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Contract.History.ID_COLUMN + " TEXT,"
                + Contract.History.TITLE_COLUMN + " TEXT,"
                + Contract.History.SITE_COLUMN + " TEXT,"
                + Contract.History.URL_COLUMN + " TEXT,"
                + Contract.History.DATE_COLUMN + " LONG"

                + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

        db.execSQL("DROP TABLE IF EXISTS " + Contract.History.TABLE_NAME);
        onCreate(db);
    }

}
