package org.softeg.northplayer.db;

import android.net.Uri;

/**
 * Created by slinkin on 03.02.14.
 */
public class Contract {
    public static final String AUTHORITY = "org.softeg.northplayer.database";
    public static final class History
    {

        public static final String TABLE_NAME="History";
        public static final String ID_COLUMN = "id";
        public static final String TITLE_COLUMN = "title";
        public static final String URL_COLUMN = "url";
        public static final String DATE_COLUMN = "shown_date";
        public static final String SITE_COLUMN = "site";

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/"+TABLE_NAME);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "."+TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY + "."+TABLE_NAME;

        public static final String DEFAULT_SORT_ORDER = DATE_COLUMN + " DESC";

    }
}
