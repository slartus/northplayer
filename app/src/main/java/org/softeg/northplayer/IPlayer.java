package org.softeg.northplayer;

/**
 * Created by slinkin on 04.02.14.
 */
public interface IPlayer {
    void playVideo(CharSequence id, CharSequence title,  CharSequence historyUrl,
                   CharSequence playUrl, int site);
    void finishActivity();

    void error(Throwable ex);
    void canceled();
}
