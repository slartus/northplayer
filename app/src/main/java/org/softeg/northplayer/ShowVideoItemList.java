package org.softeg.northplayer;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.softeg.northplayer.api.ParseResult;

/**
 * Created by slartus on 06.02.14.
 */
public class ShowVideoItemList  extends Fragment {
    static  ParseResult parseResult;
    public static ShowVideoItemList newInstance(ParseResult parseResult) {
        parseResult=parseResult;
        ShowVideoItemList f = new ShowVideoItemList();

        // Supply index input as an argument.
        Bundle args = new Bundle();


        f.setArguments(args);

        return f;
    }
}
