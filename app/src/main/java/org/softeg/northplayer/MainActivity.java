package org.softeg.northplayer;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.softeg.northplayer.api.BaseApi;
import org.softeg.northplayer.api.ParseResult;
import org.softeg.northplayer.api.VideoFormat;
import org.softeg.northplayer.api.VideoListItem;
import org.softeg.northplayer.bridges.ClipboardBridge;
import org.softeg.northplayer.bridges.SimpleCursorAdapterBridge;
import org.softeg.northplayer.common.DisplayExtensions;

import org.softeg.northplayer.common.PreferencesExtensions;
import org.softeg.northplayer.common.UriExtensions;
import org.softeg.northplayer.db.Contract;
import org.softeg.northplayer.hider.SystemUiHider;


public class MainActivity extends FragmentActivity implements IMainActivity {


    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_FULLSCREEN;
    private static final boolean AUTO_HIDE = true;
    private SystemUiHider mSystemUiHider;
    private CharSequence m_Url;

    private String mPlayedRequestUrl;


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        if (Build.VERSION.SDK_INT > 10 && getActionBar() != null) {
            getActionBar().setDisplayShowHomeEnabled(false);
            getActionBar().setDisplayHomeAsUpEnabled(false);
        }
        setPlayerViewLayoutParams(getResources().getConfiguration().orientation);

        setRequestedOrientation(PreferencesExtensions.parseInt(getApplicationContext(),
                "screen_rotation", ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED));

        systemUiHiderSetup();

        createActionMenu();
        try {

            findViewById(R.id.repeat_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    start();
                }
            });

            if (getIntent() != null && getIntent().getData() != null) {
                m_Url = getIntent().getData().toString();
            }

            start();
        } catch (Throwable ex) {
            Log.e(getContext(), ex);
        }
    }

    @Override
    public boolean onTouchEvent(android.view.MotionEvent event) {
        setHide(true);
        return super.onTouchEvent(event);
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setPlayerViewLayoutParams(newConfig.orientation);
    }

    private void setPlayerViewLayoutParams(int orientation) {
        View v = findViewById(R.id.player);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
        if (layoutParams == null) return;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutParams.setMargins(0, 0, 0, 0);
        } else {
            layoutParams.setMargins(0, 0, 0, (int) DisplayExtensions.convertDpToPixel(150, getContext()));
        }
        v.setLayoutParams(layoutParams);
        v.requestLayout();
    }

    private Boolean hide = true;

    private void systemUiHiderSetup() {
        final View contentView = findViewById(R.id.player);
        contentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                setHide(true);
                return false;
            }
        });
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (visible && AUTO_HIDE) {
                            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.player);
                            if (fragment != null) {
                                ((VideoViewPlayerFragment) fragment).showMediacontroller();
                            }
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        } else {

                        }
                    }
                });
    }

    Handler mHideHandler = new Handler();
    Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            if (hide)
                mSystemUiHider.hide();
        }
    };

    private void delayedHide(int delayMillis) {

        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void start() {
        clear();
        if (TextUtils.isEmpty(m_Url)) {
            showSelectStartDialog();
            return;
        }

        startProgress("Получение информации..");
        ParseTask parseTask = new ParseTask(this, m_Url);
        parseTask.execute();
    }

    private void showSelectStartDialog() {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setTitle(R.string.app_name);
        alertBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialogInterface.dismiss();

                finish();
            }
        });

        LinearLayout lila1 = new LinearLayout(this);
        lila1.setOrientation(LinearLayout.VERTICAL);
        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setHint("Ссылка на видео");
        lila1.addView(input);
        CharSequence text = ClipboardBridge.getText(getContext());
        if (!TextUtils.isEmpty(text)) {
            StringBuilder sb = new StringBuilder();
            if (UriExtensions.tryParse(text.toString(), sb)) {
                input.setText(sb.toString());
            }
        }
        LinearLayout linbuttons = new LinearLayout(this);
        linbuttons.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams btnParams
                = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btnParams.setMargins(0, 0, 0, 0);
        btnParams.weight = 1;
        btnParams.weight = 1;
        Button btnUrl = new Button(this);
        btnUrl.setLayoutParams(btnParams);
        Button btnHistory = new Button(this);
        btnHistory.setLayoutParams(btnParams);
        btnUrl.setText("Открыть ссылку");

        btnHistory.setText("История просмотров");

        linbuttons.addView(btnHistory);
        linbuttons.addView(btnUrl);
        lila1.addView(linbuttons);

        alertBuilder.setView(lila1);

        final AlertDialog alertDialog = alertBuilder.create();
        btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence text = input.getText().toString();
                if (TextUtils.isEmpty(text)) {
                    Toast.makeText(getContext(), "Введите ссылку", Toast.LENGTH_LONG).show();

                    return;
                }
                alertDialog.dismiss();
                startProgress("Получение информации..");
                ParseTask parseTask = new ParseTask(MainActivity.this, text);
                parseTask.execute();
            }
        });
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                showHistoryDialog();
            }
        });
        alertDialog.show();
    }

    private void showHistoryDialog() {
        try {

            final Cursor cursor = getContext().getContentResolver().query(Contract.History.CONTENT_URI,
                    null, null, null, Contract.History.DEFAULT_SORT_ORDER);
            if (cursor == null || cursor.getCount() == 0) {
                showPromoDialog();
                return;
            }

            SimpleCursorAdapter cursorAdapter = SimpleCursorAdapterBridge.createSimpleCursorAdapter(this,
                    android.R.layout.simple_list_item_2,
                    cursor, new String[]{Contract.History.TITLE_COLUMN, Contract.History.SITE_COLUMN},
                    new int[]{android.R.id.text1, android.R.id.text2});
            new AlertDialog.Builder(getContext())
                    .setTitle("История просмотров")
                    .setCancelable(true)
                    .setAdapter(cursorAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            cursor.moveToPosition(i);
                            try {
                                m_Url = cursor.getString(cursor.getColumnIndexOrThrow(Contract.History.URL_COLUMN));
                                start();
                            } catch (Exception e) {
                                Log.e(getContext(), e);
                            }
                        }
                    })

                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            dialogInterface.dismiss();
                            showSelectStartDialog();
                        }
                    })
                    .create().show();
        } catch (Throwable ex) {
            Log.e(getContext(), ex);
        }

    }

    private void showPromoDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext())
                .setTitle(R.string.app_name)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        showHistoryDialog();
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        showHistoryDialog();
                    }
                });
        alertDialogBuilder.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private Context getContext() {
        return this;
    }

    public View getProgressBar() {
        return findViewById(R.id.progressBar);
    }

    public View getProgressPanel() {
        return findViewById(R.id.progress_panel);
    }

    public TextView getProgressTextView() {
        return (TextView) findViewById(R.id.progress_text);
    }

    public TextView getErrorTextView() {
        return (TextView) findViewById(R.id.errorTextView);
    }

    public View getErrorPanel() {
        return findViewById(R.id.errorPanel);
    }

    @Override
    public void startProgress(CharSequence message) {
        getErrorPanel().setVisibility(View.GONE);
        getProgressPanel().setVisibility(View.VISIBLE);
        getProgressTextView().setText(message);
    }

    @Override
    public void stopProgress() {
        getProgressPanel().setVisibility(View.GONE);
        getErrorPanel().setVisibility(View.GONE);
    }

    @Override
    public void error(CharSequence error) {
        getProgressPanel().setVisibility(View.GONE);
        getErrorTextView().setText(error);
        getErrorPanel().setVisibility(View.VISIBLE);
    }

    public void clear() {


        if (getSupportFragmentManager().findFragmentById(R.id.player) != null)
            getSupportFragmentManager().beginTransaction()
                    .remove(getSupportFragmentManager().findFragmentById(R.id.player))
                    .commit();
        getProgressPanel().setVisibility(View.GONE);
        getErrorPanel().setVisibility(View.GONE);
    }

    @Override
    public void error(Throwable ex) {
        getProgressPanel().setVisibility(View.GONE);
        getErrorTextView().setText(Log.getMessage(ex));
        getErrorPanel().setVisibility(View.VISIBLE);
    }

    @Override
    public void canceled() {
        error("отменено");

    }

    @Override
    public void setSubtitle(CharSequence subtitle) {
        if (Build.VERSION.SDK_INT >= 11)
            if (getActionBar() != null)
                getActionBar().setSubtitle(subtitle);
    }

    public void showItemsListDialog(final CharSequence apiId, final ParseResult parseResult) {
        CharSequence[] titles = new CharSequence[parseResult.getItemsList().size()];
        int i = 0;
        for (VideoListItem item : parseResult.getItemsList()) {
            titles[i++] = item.getTitle();
        }
        new AlertDialog.Builder(getContext())
                .setTitle(parseResult.getTitle())
                .setSingleChoiceItems(titles, -1, new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(android.content.DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        startProgress("Получение информации..");
                        ParseTask parseTask = new ParseTask(MainActivity.this,
                                parseResult.getItemsList().get(i).getRequestUrl());
                        parseTask.execute();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                        if (TextUtils.isEmpty(m_Url))
                            showSelectStartDialog();
                        else
                            finish();

                    }
                })
                .create()
                .show();
    }

    public void showFormatsDialog(final CharSequence apiId, final ParseResult parseResult) {
        CharSequence[] titles = new CharSequence[parseResult.getFormats().size()];
        int i = 0;
        for (VideoFormat format : parseResult.getFormats()) {
            titles[i++] = format.getTitle();
        }
        new AlertDialog.Builder(getContext())
                .setTitle("Качество видео " + apiId)
                .setSingleChoiceItems(titles, -1, new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(android.content.DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        //createActionMenu(apiId, parseResult);

                        playVideo(apiId, parseResult, parseResult.getFormats().get(i));
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                        if (TextUtils.isEmpty(m_Url))
                            showSelectStartDialog();
                        else
                            finish();

                    }
                })
                .create()
                .show();
    }

    private void createActionMenu() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = new MenuFragment();

        ft.add(mFragment1, "f1");
        ft.commit();

    }

    public void playVideo(CharSequence apiId, ParseResult parseResult, VideoFormat videoFormat) {
        playVideo(apiId, parseResult,parseResult.getId(), parseResult.getTitle(),
                parseResult.getRequestUrl(), videoFormat.getUrl());
    }

    public void playVideo(CharSequence apiId, ParseResult parseResult,CharSequence id, CharSequence title,
                          CharSequence requestUrl, CharSequence videoUrl) {
        try {
            int position = 0;
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.player);
            if (fragment != null) {
                position = ((VideoViewPlayerFragment) fragment).getCurrentPosition();
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();

            }
            startProgress("Загрузка видео..");

            mPlayedRequestUrl = requestUrl.toString();
            VideoViewPlayerFragment details =
                    VideoViewPlayerFragment.newInstance(new PlayInfo(apiId, id, title, requestUrl,
                            videoUrl), position);
            details.ParseResult=parseResult;
            getSupportFragmentManager().beginTransaction().add(R.id.player, details).commit();

            delayedHide(AUTO_HIDE_DELAY_MILLIS);
        } catch (Exception e) {
            error(e);
        }
    }

    public String getmPlayedRequestUrl() {
        return mPlayedRequestUrl;
    }

    public void setHide(boolean hide) {
        this.hide = hide;
        if (hide)
            delayedHide(AUTO_HIDE_DELAY_MILLIS);
    }


    class ParseTask extends AsyncTask<Void, Void, ParseResult> {

        private IMainActivity mMainActivity;
        private CharSequence mUrl;
        private CharSequence mApiId;

        public ParseTask(IMainActivity mainActivity, CharSequence url) {
            mMainActivity = mainActivity;

            mUrl = url;
        }

        Throwable mEx;

        @Override
        protected ParseResult doInBackground(Void... voids) {
            try {
                BaseApi api = BaseApi.getApi(mUrl);
                mApiId = api.getApiId();
                return api.parse(mUrl);
            } catch (Exception e) {
                mEx = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(final ParseResult pResult) {
            super.onPostExecute(pResult);

            if (isCancelled()) {
                canceled();
                return;
            }

            if (mEx != null) {
                error(mEx);
                return;
            }

            mMainActivity.setTitle(pResult.getTitle());
            mMainActivity.setSubtitle(mApiId);
            if (pResult.getItemsList().size() > 0) {
                mMainActivity.showItemsListDialog(mApiId, pResult);
                return;
            }

            if (pResult.getFormats().size() > 1) {
                mMainActivity.showFormatsDialog(mApiId, pResult);
                return;
            }
            if (pResult.getFormats().size() == 1) {
                mMainActivity.playVideo(mApiId, pResult, pResult.getFormats().get(0));
                return;
            }
            mMainActivity.playVideo(mApiId, pResult,pResult.getId(), pResult.getTitle(),
                    pResult.getRequestUrl(), pResult.getVideoUrl());
        }
    }


    public static final class MenuFragment extends Fragment implements MenuItem.OnMenuItemClickListener {

        public static final String ID = "MainActivity.MenuFragment";

        public MenuFragment() {
            super();
        }

        public MainActivity getMainActivity() {
            return (MainActivity) getActivity();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);

            SubMenu subMenu = menu.addSubMenu("Ориентация экрана..").setIcon(R.drawable.ic_action_screen_rotation);
            subMenu.getItem().setOnMenuItemClickListener(this);
            subMenu.add("Портрет").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    return true;
                }
            });
            subMenu.add("Ландшафт").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    return true;
                }
            });
            subMenu.add("Авто").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    saveOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    return true;
                }
            });
            if (Build.VERSION.SDK_INT >= 11)
                subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        }

        private void saveOrientation(int orientation) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            prefs.edit().putInt("screen_rotation", orientation).commit();
            getMainActivity().setRequestedOrientation(orientation);
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            getMainActivity().setHide(false);
            return true;
        }
    }
}
