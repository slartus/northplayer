package org.softeg.northplayer;

import org.softeg.northplayer.api.ParseResult;
import org.softeg.northplayer.api.VideoFormat;

import java.util.ArrayList;

/**
 * Created by slartus on 04.02.14.
 */
public interface IMainActivity {
    void error(CharSequence error);
    void error(Throwable error);
    void canceled();
    void setTitle(CharSequence title);
    void setSubtitle(CharSequence subtitle);
    void showItemsListDialog(final CharSequence apiId, final ParseResult parseResult);
    void showFormatsDialog(CharSequence apiId,ParseResult parseResult);
    void playVideo(CharSequence apiId,ParseResult parseResult,CharSequence id,CharSequence title,
                   CharSequence requestUrl,CharSequence videoUrl);
    void playVideo(CharSequence apiId, ParseResult parseResult, VideoFormat videoFormat);
    void startProgress(CharSequence message);
    void stopProgress();

}
