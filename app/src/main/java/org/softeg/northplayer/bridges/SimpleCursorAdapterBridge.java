package org.softeg.northplayer.bridges;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.widget.SimpleCursorAdapter;

/**
 * Created by slinkin on 05.02.14.
 */
public class SimpleCursorAdapterBridge  {

    public static SimpleCursorAdapter createSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        if (Build.VERSION.SDK_INT < 11)
            return new SimpleCursorAdapter(context, layout, c, from, to);
        else
            return new SimpleCursorAdapter(context, layout, c, from, to, 0);
    }

}
