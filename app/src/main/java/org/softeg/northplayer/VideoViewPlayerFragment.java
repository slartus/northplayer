package org.softeg.northplayer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import org.softeg.northplayer.api.BaseApi;
import org.softeg.northplayer.api.ParseResult;
import org.softeg.northplayer.api.VideoFormat;
import org.softeg.northplayer.api.YouTubeAPI;
import org.softeg.northplayer.common.UriExtensions;

/**
 * Created by slinkin on 04.02.14.
 */
public class VideoViewPlayerFragment extends Fragment {

    public static ParseResult ParseResult;
    public static VideoViewPlayerFragment newInstance(PlayInfo playInfo, int seekto) {

        VideoViewPlayerFragment f = new VideoViewPlayerFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();

        args.putParcelable("playinfo", playInfo);
        args.putInt("seekto", seekto);
        f.setArguments(args);

        return f;
    }

    private int getSeekTo() {
        return Integer.parseInt(getArguments().get("seekto").toString());
    }


    private IMainActivity getMainActivity() {
        return (IMainActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.player, container, false);
        final VideoView videoView = (VideoView) v.findViewById(R.id.videoView);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer pMp) {
                if (getMainActivity() != null)
                    getMainActivity().stopProgress();
                if (getSeekTo() != 0)
                    videoView.seekTo(getSeekTo());
            }
        });

        // add listeners for finish of video
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer pMp) {

//                if (mYouTubeQueryFormatsTask != null && mYouTubeQueryFormatsTask.isCancelled())
//                    return;
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int framework_err, int impl_err) {
                int messageId = -1;

                if (framework_err == MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK) {
                    messageId = android.R.string.VideoView_error_text_invalid_progressive_playback;
                } else {
                    messageId = android.R.string.VideoView_error_text_unknown;

                }
                if (getMainActivity() != null)
                    getMainActivity().error(getContext().getString(messageId));
                return true;
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(android.os.Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PlayInfo playInfo = getPlayInfo();
        createActionMenu(playInfo.getApiId(), ParseResult);
        playVideo();

    }


    public void showMediacontroller() {
        if (mMediaController != null)
            mMediaController.show();
    }

    private PlayInfo getPlayInfo(){
        return getArguments().getParcelable("playinfo");
    }

    private void playVideo() {
        PlayInfo playInfo = getPlayInfo();
        playVideo(playInfo.getId(), playInfo.getTitle(), playInfo.getRequestUrl(),
                playInfo.getVideoUrl(), playInfo.getApiId());

    }

    public int getCurrentPosition() {
        return getVideoView().getCurrentPosition();
    }

    MediaController mMediaController;

    private void playVideo(CharSequence id, CharSequence title,
                           CharSequence historyUrl,
                           CharSequence playUrl,
                           CharSequence apiId) {
        try {
            new History(getContext()).addToHistory(id, title, historyUrl, apiId);

            VideoView videoView = getVideoView();

            videoView.setVideoURI(Uri.parse(playUrl.toString()));

            mMediaController = new MediaController(getContext());

            videoView.setMediaController(mMediaController);


            // videoView.setMediaController(new MediaController(getContext()));

            videoView.setKeepScreenOn(true);

            videoView.requestFocus();

            videoView.start();
        } catch (Throwable ex) {
            error(ex);
        }

    }


    public void finishActivity() {
        if (getActivity() != null)
            getActivity().finish();
    }


    public void error(Throwable ex) {
        if (getMainActivity() != null)
            getMainActivity().error(ex);
    }


    public void canceled() {
        if (getMainActivity() != null)
            getMainActivity().canceled();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (getVideoView() != null)
            getVideoView().stopPlayback();

        // clear the flag that keeps the screen ON

    }

    @Override
    public void onPause() {
        super.onPause();

        if (getVideoView() != null)
            getVideoView().pause();
    }

    //    @Override
//    public void onResume() {
//        super.onResume();
//        if (getVideoView() != null)
//            getVideoView().resume();
//
//    }
    private VideoView getVideoView() {
        if (getActivity() == null)
            return null;
        return (VideoView) getActivity().findViewById(R.id.videoView);
    }

    private Context getContext() {
        return getActivity();
    }


    private void createActionMenu(CharSequence apiId, ParseResult parseResult) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MenuFragment mFragment1 = (MenuFragment) fm.findFragmentByTag(MenuFragment.ID);
        if (mFragment1 != null) {
            ft.remove(mFragment1);
        }
        mFragment1 = new MenuFragment(apiId, parseResult);

        ft.add(mFragment1, MenuFragment.ID);
        ft.commit();

    }
    public static final class MenuFragment extends Fragment implements MenuItem.OnMenuItemClickListener {
        public static final String ID = "VideoViewPlayerFragment.MenuFragment";
        private CharSequence apiId;
        private ParseResult parseResult;

        public MenuFragment(CharSequence apiId, ParseResult parseResult) {
            super();

            this.apiId = apiId;
            this.parseResult = parseResult;
        }

        public MainActivity getMainActivity() {
            return (MainActivity) getActivity();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            SubMenu subMenu;
            if (parseResult.getFormats().size() > 1) {

                subMenu = menu.addSubMenu("Качество");

                subMenu.getItem().setIcon(android.R.drawable.ic_menu_view);
                subMenu.getItem().setTitle("Качество");
                subMenu.getItem().setOnMenuItemClickListener(this);

                for (final VideoFormat format : parseResult.getFormats()) {
                    subMenu.add(format.getTitle()).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            getMainActivity().playVideo(apiId, parseResult, format);
                            getMainActivity().setHide(true);
                            return true;
                        }
                    });
                }
                if (Build.VERSION.SDK_INT >= 11)
                    subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            }
            subMenu = menu.addSubMenu("Ссылка..").setIcon(R.drawable.ic_action_share);
            subMenu.getItem().setOnMenuItemClickListener(this);
            addUrlMenu(getActivity(), subMenu, getMainActivity().getmPlayedRequestUrl());
            if (Build.VERSION.SDK_INT >= 11)
                subMenu.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        public void addUrlMenu(final Context context, Menu menu, final String url) {
            menu.add("Открыть в..").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    UriExtensions.showInBrowser(context, url);
                    return true;
                }
            });

            menu.add("Поделиться ссылкой").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    UriExtensions.shareIt(context, url);
                    return true;
                }
            });

            menu.add("Скопировать ссылку").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getMainActivity().setHide(true);
                    UriExtensions.copyLinkToClipboard(context, url);
                    return true;
                }
            });
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            getMainActivity().setHide(false);
            return true;
        }
    }
}
